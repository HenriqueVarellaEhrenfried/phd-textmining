from src.LinguisticFeatures import LinguisticFeatures

Ling = LinguisticFeatures()

nlp = Ling.generateNLP('It is so sad that I won the lottery')

extracted_pos = Ling.extractPOS(nlp)

dependecy = Ling.dependencyParse(nlp)

#print("EXTRACTED POS >> ", extracted_pos)

#print("Dependency >> ", dependecy)

unified = Ling.unifyStatistics(extracted_pos, dependecy)

for u in unified:
    Ling.printUnified(u)
    print("-------------------------------------")
