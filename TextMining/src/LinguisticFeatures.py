import spacy

class LinguisticFeatures:
    """This class extracts contains methods to manipulate a text"""  
    def __init__(self):
        # Loads the model that will be used
        self.nlp = spacy.load('en_core_web_sm')

    def generateNLP(self, sentence):
        # Generates the nlp version of the sente for further processing
        return self.nlp(sentence)

    def extractPOS(self, NlpSentence):
        """Extract all parts of speech """
        result = []
        POS = {}
        for token in NlpSentence:
            POS['Text'] = token.text
            POS['Lemma'] = token.lemma_
            POS['POS'] = token.pos_
            POS['TAG'] = token.tag_
            POS['DEP'] = token.dep_
            POS['Shape'] = token.shape_
            POS['Is Alpha'] = token.is_alpha
            POS['Is Stop'] = token.is_stop
            result.append(POS)
            POS = {}
        return result

    def dependencyParse(self, NlpSentence):
        """Generate the dependency in sentence"""
        result = []
        DEP = {}
        for token in NlpSentence:
            DEP['Text'] = token.text
            DEP['DEP'] = token.dep_
            DEP['Head'] = token.head
            DEP['Head Text'] = token.head.text
            DEP['Head POS'] = token.head.pos_
            DEP['Children'] = [child for child in token.children]
            result.append(DEP)
            DEP = {}
        return result

    def unifyStatistics(self, pos, dependency):
        """Unify output of dependencyParse and extractPOS"""
        posLength = len(pos)
        dependencyLength = len(dependency)
        result = [] 
        if posLength == dependencyLength:
            unified = {}
            for i in range(0, posLength):
                # The above line is supported in Python >= 3.5
                unified = {**pos[i], **dependency[i]}
                result.append(unified)
        return result

    def printUnified(self, dictionary):
        """Print a unified dictionary in a better shape"""
        print('Text: ', dictionary['Text'])
        print('Lemma: ', dictionary['Lemma'])
        print('POS: ', dictionary['POS'])
        print('TAG: ', dictionary['TAG'])
        print('DEP: ', dictionary['DEP'])
        print('Shape: ', dictionary['Shape'])
        print('Is Alpha: ', dictionary['Is Alpha'])
        print('Is Stop: ', dictionary['Is Stop'])
        print('Head: ', dictionary['Head'])
        print('Head Text: ', dictionary['Head Text'])
        print('Head POS: ', dictionary['Head POS'])
        print('Children: ', dictionary['Children'])